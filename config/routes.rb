Rails.application.routes.draw do

  root 'application#index'

  post '/session/signin', to: 'sessions#signin'
  get '/session/signout', to: 'sessions#signout'
  
  get '/home', to: 'application#home'
  get '/about', to: 'application#about'
  get '/account', to: 'application#account'
  get '/profile/:profile_id', to: 'application#profile', as: 'profile'
  get '/following', to: 'application#following'
  get '/followers', to: 'application#followers'
  post '/search', to: 'application#search'

  namespace :api do
    namespace :v1 do
      resources :feed, only: [:index]
      resources :users, only: [:index, :create, :show, :update, :destroy] do
        resources :posts, only: [:index, :create, :destroy]
        resources :relationships, only: [:create, :destroy]
        resources :notifications, only: [:index, :update, :destroy]

        get '/following_list', to: 'relationships#following_list'
        get '/followers_list', to: 'relationships#followers_list'
        delete '/follower_delete/:profile_id', to: 'relationships#follower_delete'
        get '/relationship_test/:profile_id', to: 'relationships#test', as: 'relationship_test'
      end
    end
  end
  
end
