class ChangePostsColumns < ActiveRecord::Migration
  def change
    rename_column :posts, :type, :format
  end
end
