class ApplicationController < BaseController

  before_action :force_authenticate, except: [:index, :profile, :about]

  def index
    if !is_authenticated?
      render :index
    else
      redirect_to home_path
    end
  end

  def home
    render :home
  end

  def search
    @users = User.where("name like ? or email like ?", "%#{params[:search][:query]}%", "%#{params[:search][:query]}%")
    render :search
  end

  def account
    render :account
  end

  def profile
    @profile = User.where(id: params[:profile_id]).first

    render :profile
  end

  def following
    render :following
  end

  def followers
    render :followers
  end

  def about
    render :about
  end

end