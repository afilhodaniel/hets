class SessionsController < ApplicationController

  skip_before_action :force_authenticate
  
  respond_to :json
  
  def signin
    user = User.where(email: params[:email], password: User.hash_password(params[:password])).first

    if user
      session[:current_user] = user

      render :success
    else
      render :errors
    end
  end

  def signout
    session[:current_user] = nil if is_authenticated?

    redirect_to root_path
  end

end