module Api
	module V1
    class RelationshipsController < Api::V1::BaseController

      before_action :send_notification, only: [:create]

      def test
        @relationship = Relationship.where(follower_id: params[:user_id], followed_id: params[:profile_id]).first
        
        if @relationship
          render :test_success
        else
          render :test_false
        end
      end

      def following_list
        @relationships = @current_user.relationships.where(follower_id: @current_user.id)

        render :following_list
      end

      def followers_list
        @relationships = Relationship.where(followed_id: @current_user.id)

        render :followers_list
      end

      def follower_delete
        relationship = Relationship.where(follower_id: @current_user.id, followed_id: params[:profile_id]).first
        relationship.destroy
        render :success
      end
      
      private

        def relationship_params
          params.require(:relationship).permit(:follower_id, :followed_id)
        end

        def query_params
          params.permit(:follower_id, :followed_id)
        end

        def send_notification
          Notification.create(message: "#{@current_user.name} agora está seguindo você", user_id: params[:relationship][:followed_id])
        end

    end
  end
end