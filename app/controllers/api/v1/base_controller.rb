module Api
  module V1
    class BaseController < ApplicationController
      
      before_action :set_resource, only: [:show, :update, :destroy]

      respond_to :json

      # GET /api/v1/{plural_resource_name}.json
      def index
        plural_resource_name = "@#{resource_name.pluralize}"
        
        resources = resource_class.where(query_params)
        
        instance_variable_set(plural_resource_name, resources)

        render 'index.json'
      end

      # GET /api/v1/{plural_resource_name}/:id.json
      def show
        respond_with get_resource
      end

      # POST /api/v1/{plural_resource_name}.json
      def create
        set_resource(resource_class.new(resource_params))

        if get_resource.save
          instance_variable_set("@#{resource_name}", get_resource)
          
          render :success
        else
          instance_variable_set('@errors', get_resource.errors)
          
          render :errors
        end
      end

      # PATCH/PUT /api/v1/{plural_resource_name}/:id.json
      def update
        if get_resource.update(resource_params)
          render :success
        else
          instance_variable_set('@errors', get_resource.errors)

          render :errors
        end
      end

      # DELETE /api/v1/{plural_resource_name}/:id.json
      def destroy
        get_resource.destroy
        render :success
      end

      private

        def get_resource
          instance_variable_get("@#{resource_name}")
        end

        def query_params
          {}
        end

        def page_params
          params.permit(:page, :page_size)
        end

        def resource_class
          @resource_class ||= resource_name.classify.constantize
        end

        def resource_name
          @resource_name ||= self.controller_name.singularize
        end

        def resource_params
          @resource_params ||= self.send("#{resource_name}_params")
        end

        def set_resource(resource = nil)
          resource ||= resource_class.find(params[:id])
          instance_variable_set("@#{resource_name}", resource)
        end

    end
  end
end