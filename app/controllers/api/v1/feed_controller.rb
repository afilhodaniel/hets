module Api
	module V1
    class FeedController < Api::V1::BaseController
      
      def index
        where = [@current_user.id]

        @current_user.relationships.each do |relationship|
          where.push relationship.followed_id
        end

        @posts = Post.where(user_id: where).page(page_params[:page]).per(page_params[:page_size])

        render 'index.json'
      end

    end
  end
end