module Api
  module V1
    class UsersController < Api::V1::BaseController

      skip_before_action :force_authenticate, only: [:create]
      
      private

        def user_params
          params.require(:user).permit(:name, :bio, :avatar, :email, :password)
        end

        def query_params
          params.permit(:name, :bio, :avatar, :email, :password)
        end

    end
  end
end