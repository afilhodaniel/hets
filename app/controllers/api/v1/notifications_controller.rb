module Api
  module V1
    class NotificationsController < Api::V1::BaseController

      before_action :new_notifications_count, only: [:index]
      
      private

        def notification_params
          params.require(:notification).permit(:user_id, :message, :read)
        end

        def query_params
          params.permit(:user_id, :message, :read)
        end

        def new_notifications_count
          @new_notifications_count = Notification.where(user_id: @current_user.id, read: false).count
        end

    end
  end
end