module Api
  module V1
    class PostsController < Api::V1::BaseController

      def index
        plural_resource_name = "@#{resource_name.pluralize}"

        resources = resource_class.where(query_params).page(page_params[:page]).per(page_params[:page_size])

        instance_variable_set(plural_resource_name, resources)

        render 'index.json'
      end
      
      private

        def post_params
          params.require(:post).permit(:user_id, :content)
        end

        def query_params
          params.permit(:user_id, :content)
        end

    end
  end
end