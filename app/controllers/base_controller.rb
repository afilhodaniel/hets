class BaseController < ActionController::Base

  before_action :set_current_user

  private

    def is_authenticated?
      if !session[:current_user].nil?
        return true
      else
        return false
      end
    end

    def force_authenticate
      redirect_to root_path if !is_authenticated?
    end

    def set_current_user
      if is_authenticated?
        @current_user = User.where(id: session[:current_user]['id']).first
      else
        @current_user = nil
      end
    end

end