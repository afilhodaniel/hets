window.HetsApp.controller 'NotificationsController', ($scope, $http) ->

  months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
  days = ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo']

  $scope.showNotifications = false

  $scope.notifications = []
  $scope.new_notifications_count = 0

  load = ->
    method = 'get'
    url = '/api/v1/users/' + current_user_id + '/notifications'

    $http(
      method: method
      url: url
    ).success (data) ->
      if data.new_notifications_count > 0
        $scope.new_notifications_count = data.new_notifications_count
      else
        $scope.new_notifications_count = 0

      if data.notifications.length > 0
        $scope.notifications = data.notifications

  $scope.openNotifications = ->
    $scope.showNotifications = !$scope.showNotifications

  $scope.date = (date) ->
    date = new Date(date)

    days[date.getDay() - 1] + ', ' + date.getDate() + ' de ' + months[date.getMonth()].toLowerCase() + ' de ' + date.getFullYear() + ' às ' + date.getHours() + ':' + date.getUTCMinutes() + ' horas'

  $scope.update = (notification) ->
    notification.read = true

    method = 'put'
    url = '/api/v1/users/' + current_user_id + '/notifications/' + notification.id

    $http(
      method: method
      url: url + '.json'
      data: notification
      headers: {
        'Content-Type': 'application/json'
      }
    ).success (data) ->
      load()

  $scope.delete = (notification) ->
    method = 'delete'
    url = '/api/v1/users/' + current_user_id + '/notifications/' + notification.id

    $http(
      method: method
      url: url + '.json'
    ).success (data) ->
      load()

  load()