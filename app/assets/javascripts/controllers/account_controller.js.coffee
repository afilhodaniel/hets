window.HetsApp.controller 'AccountController', ($scope, Upload) ->

  $scope.success = false
  $scope.showErrors = false

  $scope.account = {
    name: ''
    avatar: ''
    bio: ''
    email: ''
    password: ''
  }

  $scope.errors = {
    name: []
    avatar: []
    bio: []
    email: []
    password: []
  }

  $scope.proccess = (e) ->
    e.preventDefault()

    method = $('.account-form').find('input[name=_method]').val()
    url = $('.account-form').attr('action') + '.json'

    Upload.upload(
      method: method
      url: url
      data: { user: $scope.account }
      file: $scope.account.avatar
      fielFormDataName: 'user[avatar]'
    ).then (data) ->
      if !data.data.success
        $scope.showErrors = true
      
        if data.data.errors.name and data.data.errors.name.length > 0
          $scope.errors.name = data.data.errors.name
        else
          $scope.errors.name = []

        if data.data.errors.avatar and data.data.errors.avatar.length > 0
          $scope.errors.avatar = data.data.errors.avatar
        else
          $scope.errors.avatar = null

        if data.data.errors.email and data.data.errors.email.length > 0
          $scope.errors.email = data.data.errors.email
        else
          $scope.errors.email = []

        if data.data.errors.password and data.data.errors.password.length > 0
          $scope.errors.password = data.data.errors.password
        else
          $scope.errors.password = []
      else
        $scope.success = true
        $scope.showErrors = false
        $scope.errors = {}
        $scope.account.password = ''