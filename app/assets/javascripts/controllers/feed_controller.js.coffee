window.HetsApp.controller 'FeedController', ($scope, $http) ->

  page = 1
  months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
  days = ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo']

  $scope.showTotalHets = false
  $scope.showLoadMore = true
  $scope.showErrors = false

  $scope.current_user_id = current_user_id
  
  $scope.errors = {
    content: []
  }

  $scope.post = {
    content: ''
    user_id: ''
  }

  $scope.posts = []

  load = ->
    url = '/api/v1/feed?page=' + page + '&page_size=5'

    $http(
      method: 'get'
      url: url
    ).success (data) ->
      if data.posts.length > 0
        $scope.posts = data.posts

        page++
      else
        $scope.showLoadMore = false

  $scope.proccess = (e) ->
    e.preventDefault()

    method = $('.posts-form').attr('method')
    url = $('.posts-form').attr('action')

    $http(
      method: method
      url: url + '.json'
      data: $scope.post
      headers: {
        'Content-Type': 'application/json'
      }
    ).success (data) ->
      if data.success
        $scope.showErrors = false

        if $scope.posts.length > 0
          $scope.posts.unshift data.post
        else
          $scope.posts.push data.post

        $scope.post.content = ''
      else
        $scope.showErrors = true

        if data.errors.content and data.errors.content.length > 0
          $scope.errors.content = data.errors.content
        else
          $scope.errors.content = []

  $scope.verify = ->
    if $scope.post.content.length > 70
      $scope.post.content = $scope.post.content.slice(0, 70)

  $scope.date = (date) ->
    date = new Date(date)

    days[date.getDay() - 1] + ', ' + date.getDate() + ' de ' + months[date.getMonth()].toLowerCase() + ' de ' + date.getFullYear() + ' às ' + date.getHours() + ':' + date.getUTCMinutes() + ' horas'

  $scope.more = ->
    url = '/api/v1/feed?page=' + page + '&page_size=5'

    $http(
      method: 'get'
      url: url
    ).success (data) ->
      if data.posts.length > 0
        data.posts.forEach (elem) ->
          $scope.posts.push elem

        page++
      else
        $scope.showLoadMore = false
        $scope.showTotalHets = true

  $scope.delete = (post) ->
    method = 'delete'
    url = '/api/v1/users/' + current_user_id + '/posts/' + post.id

    $http(
      method: method
      url: url + '.json'
    ).success (data) ->
      $($('.posts-list-item')[$scope.posts.indexOf(post)]).fadeOut()

  load()