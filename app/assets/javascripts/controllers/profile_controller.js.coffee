window.HetsApp.controller 'ProfileController', ($scope, $http) ->

  $scope.showTotalHets = false
  $scope.showLoadMore = true
  $scope.relationship = {}

  $scope.current_user_id = current_user_id

  page = 1
  $scope.posts = []

  load = ->
  	method = 'get'
  	url = '/api/v1/users/' + $('.profile_id').val() + '/posts?page=' + page + '&page_size=5'

    # Load user posts
  	$http(
      method: method
      url: url
  	).success (data) ->
      if data.posts.length > 0
        data.posts.forEach (elem) ->
          $scope.posts.push elem

        page++
      else
        $scope.showLoadMore = false

  loadRelationship = ->
    method = 'get'
    url = '/api/v1/users/' + current_user_id + '/relationship_test/' + $('.profile_id').val()

    $http(
      method: method
      url: url + '.json'
    ).success (data) ->
      $scope.relationship = data

  $scope.updateRelationship = ->
    method = ''
    url = ''
    data = {}

    if $scope.relationship.test
      # Unfollow
      method = 'delete'
      url = '/api/v1/users/' + current_user_id + '/relationships/' + $scope.relationship.id
    else
      # Follow
      method = 'post'
      url = '/api/v1/users/' + current_user_id + '/relationships'
      data = {relationship: {
        follower_id: current_user_id
        followed_id: $('.profile_id').val()
      }}

    $http(
        method: method
        url: url + '.json'
        data: data
      ).success (data) ->
        loadRelationship()

  $scope.more = ->
    url = '/api/v1/users/' + $('.profile_id').val() + '/posts?page=' + page + '&page_size=5'

    $http(
      method: 'get'
      url: url
    ).success (data) ->
      if data.posts.length > 0
        data.posts.forEach (elem) ->
          $scope.posts.push elem

        page++
      else
        $scope.showLoadMore = false
        $scope.showTotalHets = true

  $scope.delete = (post) ->
    method = 'delete'
    url = '/api/v1/users/' + current_user_id + '/posts/' + post.id

    $http(
      method: method
      url: url + '.json'
    ).success (data) ->
      $($('.posts-list-item')[$scope.posts.indexOf(post)]).fadeOut()

  load()
  loadRelationship()