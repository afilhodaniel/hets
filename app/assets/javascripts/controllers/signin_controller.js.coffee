window.HetsApp.controller 'SigninController', ($scope, $http) ->

  $scope.errors = false
  
  $scope.user = {
    email: ''
    password: ''
  }

  $scope.process = (e) ->
    e.preventDefault()

    method = $('.signin-form').attr('method')
    url = $('.signin-form').attr('action')

    $http(
      method: method
      url: url + '.json'
      data: $scope.user
      headers: {
        'Content-Type': 'application/json'
      }
    ).success (data) ->
      if !data.success
        $scope.errors = true
      else
        window.location.href = data.redirect_to
