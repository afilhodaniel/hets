window.HetsApp.controller 'FollowingController', ($scope, $http) ->

  $scope.showNoUsers = false
  $scope.relationships = []

  load = ->
    method = 'get'
    url = '/api/v1/users/' + current_user_id + '/following_list'

    $http(
      method: method
      url: url + '.json'
    ).success (data) ->
      $scope.relationships = data.relationships
      
      if data.relationships.length > 0
        $scope.showNoUsers = false
      else
        $scope.showNoUsers = true

  $scope.updateRelationship = (relationship) ->
    method = ''
    url = ''
    data = {}

    if relationship.test
      # Unfollow
      method = 'delete'
      url = '/api/v1/users/' + current_user_id + '/relationships/' + relationship.id
    else
      # Follow
      method = 'post'
      url = '/api/v1/users/' + current_user_id + '/relationships'
      data = {relationship: {
        follower_id: current_user_id
        followed_id: relationship.user.id
      }}

    $http(
        method: method
        url: url + '.json'
        data: data
      ).success (data) ->
        load()

  load()