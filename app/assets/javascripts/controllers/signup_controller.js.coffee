window.HetsApp.controller 'SignupController', ($scope, $http) ->

  $scope.success = false
  
  $scope.user = {
    name: ''
    email: ''
    password: ''
  }

  $scope.errors = {
    name: []
    email: []
    password: []
  }

  $scope.process = (e) ->
    e.preventDefault()

    method = $('.signup-form').attr('method')
    url = $('.signup-form').attr('action')

    $http(
      method: method
      url: url + '.json'
      data: $scope.user
      headers: {
        'Content-Type': 'application/json'
      }
    ).success (data) ->
      if !data.success
        $scope.success = false

        if data.errors.name and data.errors.name.length > 0
          $scope.errors.name = data.errors.name
        else
          $scope.errors.name = []

        if data.errors.email and data.errors.email.length > 0
          $scope.errors.email = data.errors.email
        else
          $scope.errors.email = []

        if data.errors.password and data.errors.password.length > 0
          $scope.errors.password = data.errors.password
        else
          $scope.errors.password = []
      else
        $scope.success = true
        $scope.user = {}
        $scope.errors = {}