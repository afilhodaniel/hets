json.post do
  json.id         @post.id
  json.created_at @post.created_at
  json.updated_at @post.updated_at
  json.content    @post.content
  json.user do
    json.id       @post.user.id
    json.name     @post.user.name
    json.avatar   @post.user.avatar
    json.email    @post.user.email
  end
end