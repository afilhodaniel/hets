json.user do
  json.id     @user.id
  json.avatar @user.avatar
  json.name   @user.name
  json.bio    @user.bio
  json.email  @user.email
end