json.relationships @relationships do |relationship|
  json.id          relationship.id
  json.created_at  relationship.created_at
  json.updated_at  relationship.updated_at
  json.follower_id relationship.follower_id
  json.followed_id relationship.followed_id
  json.test        true
  json.user do
    json.id        relationship.followed.id
    json.avatar    relationship.followed.avatar.url(:avatar)
    json.name      relationship.followed.name
    json.email     relationship.followed.email
    json.bio       relationship.followed.bio
  end
end