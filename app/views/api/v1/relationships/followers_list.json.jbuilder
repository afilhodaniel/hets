json.relationships @relationships do |relationship|
  json.id          relationship.id
  json.created_at  relationship.created_at
  json.updated_at  relationship.updated_at
  json.follower_id relationship.follower_id
  json.followed_id relationship.followed_id
  json.user do
    json.id        relationship.follower.id
    json.avatar    relationship.follower.avatar.url(:avatar)
    json.name      relationship.follower.name
    json.email     relationship.follower.email
    json.bio       relationship.follower.bio
  end
end