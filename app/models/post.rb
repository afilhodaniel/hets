class Post < ActiveRecord::Base
  belongs_to :user

  default_scope { order('created_at DESC') }
  
  validates :content, presence: true, length: 0..70
end
