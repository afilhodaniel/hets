class User < ActiveRecord::Base
  has_many :posts, dependent: :destroy
  has_many :relationships, dependent: :destroy, foreign_key: :follower_id
  has_many :notifications, dependent: :destroy

  before_save :hash_password, if: proc{|user| !user.password.blank?}

  validates :name, presence: true
  validates :bio, presence: false
  validates :email, presence: true, uniqueness: true
  validates :password, presence: true

	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>", avatar: "48x48>" }, default_url: "http://heets.herokuapp.com/assets/avatar-9d5abf50cb8b8e80a90448c1d91591dcab5f2a4553733c438130e2bf8d094230.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  private 

    def hash_password
      self.password = Digest::SHA2::hexdigest(password)
    end

    def self.hash_password(password = nil)
      Digest::SHA2::hexdigest(password) if !password.nil?
    end

end