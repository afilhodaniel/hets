class Relationship < ActiveRecord::Base
	belongs_to :follower, class_name: 'User'
	belongs_to :followed, class_name: 'User'

	default_scope { order('created_at DESC') }
end
